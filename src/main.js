// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import App from './App'

import Template from './components/Template'
import AjaxTest from './components/AjaxTest'
import Home from './components/Home'
import CopyTemplate from './components/CopyTemplate'
import ListingTemplate from './components/ListingTemplate'
import StreamTemplate from './components/StreamTemplate'
import SettingsTemplate from './components/SettingsTemplate'
import LoginTemplate from './components/LoginTemplate'
import ResetTemplate from './components/ResetTemplate'

Vue.use(VueRouter)
Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  }
})

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/template',
    component: Template
  },
  {
    path: '/listing-template',
    component: ListingTemplate
  },
  {
    path: '/stream-template',
    component: StreamTemplate
  },
  {
    path: '/settings-template',
    component: SettingsTemplate
  },
  {
    path: '/login-template',
    component: LoginTemplate
  },
  {
    path: '/reset-template',
    component: ResetTemplate
  },
  {
    path: '/ajax-test',
    component: AjaxTest
  },
  {
    path: '/copy-template',
    component: CopyTemplate
  }
]

/* eslint-disable no-new */
const router = new VueRouter({
  routes
})

new Vue({
  router,
  store,
  el: '#app',
  template: '<App/>',
  components: { App }
})
